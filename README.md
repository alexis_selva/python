These are the programming assignments relative to Python for everybody - getting started with python (1st part)

- Week 2: Installing and Using Python 
- Week 3: Chapter One: Why We Program (continued)
- Week 4: Chapter Two: Variables and Expressions
- Week 5: Chapter Three: Conditional Code
- Week 6: Chapter Four: Functions
- Week 7: Chapter Five: Loops and Iteration

For more information, I invite you to have a look at https://www.coursera.org/learn/python

